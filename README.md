# Juego de Conway 
Una version de la vida de Conway escrita en PHP para ser ejecutada en un navegador

## Descripcion
El archivo "index.php" se encarga de imprimir un tablero con celulas vivas y muertas de manera aleatoria. El juego completo consta de 6 iteraciones en las que se modifica el estado de las celulas y se actualiza el tablero. Usando los botones el usuario puede "Empezar de nuevo" para ver otra partida y decidir si quiere ver todas las iteraciones o solo la final.

## Uso
Para poder observar el juego de Conway sigue estos pasos:
1. Descarga un servidor local o usa uno en la nube. En mi caso uso XAMPP.
2. Pega todo el contenido del repositorio en una carpeta donde se pueda ejecutar. En mi caso he creado una carpeta nueva "Conway" y la he colocado en la siguiente ruta: C:\xampp\htdocs\Conway 
3. En el caso de que uses el servidor local de XAMPP tienes que encender el modulo de Apache.
4. Elige un navegador y busca la ruta en la que se encuentren los archivos. En mi caso: http://127.0.0.1/conway
5. Pulsa intro y el archivo "index.php" del repositorio se ejecutara y podras ver automaticamente el Juego de Conway.

