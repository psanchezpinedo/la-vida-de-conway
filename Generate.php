<?php


class Generate{

	var $matrix = array();
	var $size = 10;
	var $count = 0;

 	function Mincount(){
		//Comprobamos que hay 3 celulas min. vivas //Viva = 1//Muerta = 0
 		$this->count = 0;
		foreach ($this->matrix as $key) {
			foreach($key as $value){
				if($value == 1){ 
					$this->count+=1;
				}
			}
		}
		return $this->count;
	}

 	function MinAlive(){
		do{
		$tam = $this->size;
		$this->count = $this->Mincount();
			if($this->count<3){ //Ponemos los 1s en posiciones aleatorias
				$this->matrix[rand(0,$tam-1)][rand(0,$tam-1)] = rand(0,1);
			}
			
		}while($this->count<3);
		
		return $this->matrix;
	}

	function tablero(){
		//Creamos el tablero
		for($i = 0; $i < $this->size; $i++) {
		    for($j = 0; $j < $this->size; $j++) {
		            $this->matrix[$i][$j] = rand(0,1); 
		    }
		}
		$matrix = $this->MinAlive();
		return $matrix;
	}

	function upgrade($original){
		echo '<table border="1">';
		for($i = 0; $i < $this->size; $i++) {
			echo '<tr>';
			for($j = 0; $j < $this->size; $j++) {
			    if($original[$i][$j] == 0){
			        echo '<td class = "muerta"></td>';
			    }else{
					echo '<td class = "viva"></td>';
			    }
			}
			echo '</tr>';
		}
		echo '</table>';
	return;
	}
}


?>