<?php


class Games{

	var $size = 10;

	function testing($original){
		$new = $original;
		//Recorremos el tablero
			for ($u=0;$u < $this->size; $u++){
				for ($v=0;$v < $this->size;$v++){
				    $counter=0;
				    //Estudiamos sus vecinos
					for ($i = $u - 1; $i <= $u + 1; $i++) {
						if ($i < 0 || $i >= $this->size) { //Se sale de la matriz
							continue;
						}
						for ($j = $v - 1; $j <= $v + 1; $j++) {
							if ($j < 0 || $j >= $this->size) {//Se sale de la matriz
								continue;
							}
							if($j == $v && $i == $u){ //Celda a actualizar
								continue;
							}
							if ($original[$i][$j] == 1) {
								$counter+=1;
							}						       
						}
					}

					//Modificamos el valor de la nueva matrix
					if($counter >=3 && $original[$u][$v] == 0 ){
				  		$new[$u][$v] = 1;
					}

					if($original[$u][$v] == 1 ){
						if($counter >=3 ){
							$new[$u][$v] = 1;
						}else{
						$new[$u][$v] = 0;
						}
					}	
				}
			}	
		return $new;
	}
}
?>