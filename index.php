
<!DOCTYPE html>
<html lang="en">
<head>
  	<title>La vida de Conway</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Bootstrap -->
  	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  	<!--Estilo -->
  	<link rel="stylesheet" href="css/style.css" type="text/css">
  	<!-- Fuente -->
 	<link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet"> 
  	<!-- scripts -->
	<script src="js/jquery.min.js"></script>
	<script src="js/animate.js"></script>
</head>
<body>

	<?php 
	include ("Generate.php"); 
	$generada = new Generate;
	include("Games.php");
	$actualizada = new Games;
	$iter=6;
	?>

	<div class = "container">
		<div class = "title"><h1> - La vida de Conway - </h1></div>
			<?php $original = $generada->tablero(); //Generamos el tablero original
			for($r = 0; $r <= $iter; $r++){ //Imprimimos los diferentes tableros 
				if($r == 0){?>
					<div class = "tablero col-md-4">
						 <h3>Generación inicial</h3>
						<?php $generada->upgrade($original); //Imprimimos la jugada inicial ?>
					</div>

					<div class= "col-md-4 info">
						<div class = "funciones ">
							<button class="btn btn-dark " onClick="window.location.reload()">Empezar de nuevo</button>
							<div class="opciones">
								<h3>Mostrar generaciones:</h3>
								<button class="btn btn-dark active" id="show">Todas</button>
								<button class="btn btn-dark " id="hide">Final</button>
							</div>
						</div>

						<div class="row">
						<div class = "col-md-2 col-xs-2"><div class = "viva"></div></div>
						<div class = "col-md-10 col-xs-10"><h3>Célula viva</h3></div>
						</div>
						<div class="row">
						<div class = "col-md-2 col-xs-2"><div class ="muerta"></div></div>
						<div class = "col-md-10 col-xs-10"><h3>Célula muerta</h3></div>
						</div>
					</div>
				
				<?php
				}elseif($r == $iter){ //Imprime solo la ultima iteracion ?>
					<div class = "tablero col-md-4">
					<?php echo '<h3>Generación nº ' . $r . '</h3>';
					$original = $check; //Nueva original: matriz de la anterior jugada
					$generada->upgrade($original); //Imprimimos la nueva jugada ?>
					</div>

				<?php
				}else{ //Imprime el resto de iteraciones ?>
					<div class = "tablero col-md-4 hiding"> 
					<?php echo '<h3>Generación nº ' . $r . '</h3>';
					$original = $check; //Nueva original: matriz de la anterior jugada
					$generada->upgrade($original); //Imprimimos la nueva jugada ?>
					</div>
				<?php 
				}
				$check = $actualizada->testing($original); //Testeamos las celdas y modificamos
			}
		?>
	</div>
</body>
</html>
